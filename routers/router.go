package routers

import (
	"github.com/amlwwalker/willkeith/controllers"
	"github.com/astaxie/beego"
)

func init() {
    beego.Router("/", &controllers.IndexController{})
    beego.Router("/gallery", &controllers.GalleryController{})
    beego.Router("/pricing", &controllers.PricingController{})
    beego.Router("/jobs", &controllers.JobsController{})
}
