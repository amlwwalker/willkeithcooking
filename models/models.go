package models

import (
 	"gopkg.in/yaml.v2"
 	"path/filepath"
 	"io/ioutil"
 	"log"
 )

type Element struct {
	Title string `yaml:"Title"`
	Description string `yaml:"Description"`
}
type Page struct {
	Title string `yaml:"Title"`
	PrefixText string `yaml:"PrefixText"`
	Elements []Element `yaml:"Elements"`
}
type Config struct {
	Phone string `yaml:"Phone"`
	Website string `yaml:"Website"`
	Email string `yaml:"Email"`
	About Page `yaml:"About"`
	Jobs Page `yaml:"Jobs"`
}

var CONFIG Config

func LoadConfig() {
		// CONFIG.Prefix = "hello"
	filename, _ := filepath.Abs("./config.yml")
    yamlFile, err := ioutil.ReadFile(filename)

    if err != nil {
        panic(err)
    }
    log.Println(string(yamlFile))
    err = yaml.Unmarshal(yamlFile, &CONFIG)
    if err != nil {
        panic(err)
    }

    log.Printf("Value: %#v\n", CONFIG)
}