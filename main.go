package main

import (
	_ "github.com/amlwwalker/willkeith/routers"
	"github.com/amlwwalker/willkeith/models"
	"github.com/astaxie/beego"
)

func init() {
	models.LoadConfig()
}
func main() {
	beego.Run()
}

