package controllers

import (
	"github.com/astaxie/beego"
	"github.com/amlwwalker/willkeith/models"
)

type JobsController struct {
	beego.Controller
}

func (c *JobsController) Get() {
	// models.LoadConfig()
	c.Data["Phone"] = models.CONFIG.Phone
	c.Data["Website"] = models.CONFIG.Website
	c.Data["Email"] = models.CONFIG.Email
	c.Data["Title"] = models.CONFIG.Jobs.Title
	c.Data["PrefixText"] = models.CONFIG.Jobs.PrefixText
	c.Data["Jobs"] = models.CONFIG.Jobs.Elements

	c.TplName = "jobs.tpl"
}
