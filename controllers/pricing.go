package controllers

import (
	"github.com/astaxie/beego"
)

type PricingController struct {
	beego.Controller
}

func (c *PricingController) Get() {
	c.Data["Title"] = "Pricing"
	c.Data["Phone"] = "+44-123-4567-890"
	c.Data["Website"] = "willkeithcooking.com"
	c.Data["Email"] = "willkeith88@gmail.com"
	c.TplName = "pricing.tpl"
}
