package controllers

import (
	"github.com/astaxie/beego"
	"github.com/amlwwalker/willkeith/models"
)

type IndexController struct {
	beego.Controller
}

func (c *IndexController) Get() {
	// models.LoadConfig()
	c.Data["Phone"] = models.CONFIG.Phone
	c.Data["Website"] = models.CONFIG.Website
	c.Data["Email"] = models.CONFIG.Email
	c.Data["Title"] = models.CONFIG.About.Title
	c.Data["PrefixText"] = models.CONFIG.About.PrefixText
	c.TplName = "index.tpl"
}
