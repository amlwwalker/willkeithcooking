package controllers

import (
	"github.com/astaxie/beego"
	"path/filepath"
	"strings"
	"fmt"
	"os"
)

type GalleryController struct {
	beego.Controller
}

type Image struct {
	Path string
	Description string
}
func (c *GalleryController) Get() {
	c.Data["Title"] = "Gallery"
	c.Data["Phone"] = "+44-123-4567-890"
	c.Data["Website"] = "willkeithcooking.com"
	c.Data["Email"] = "willkeith88@gmail.com"
	c.TplName = "gallery.tpl"
	c.Data["Images"] = traverseImages("./static/img/gallery/")
	fmt.Println(c.Data)
	fmt.Println(c.Data["Images"])
}

func traverseImages(directory string) []Image {
	var images []Image
	filepath.Walk(directory, func(path string, info os.FileInfo, err error) error {
		if strings.Contains(path, ".jpg") {
			var tmp Image
			tmp.Path = path
			absFileName := strings.Replace(path, directory, "", -1)
			relFileName := strings.Split(absFileName, "/")
			fileNameItself := strings.Split(relFileName[len(relFileName) - 1], ".")
			tmp.Description = fileNameItself[0]
			images = append(images, tmp)
		}
		return nil
	})
	return images
}