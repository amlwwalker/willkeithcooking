<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<title>{{.Title}}</title>
<meta name="description" content="We provide cooks and assistant cooks for full time, part time, temporary and permanent positions to private clients both in the UK and abroad.">
<meta name="keywords" content="">

<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="content-language" content="en">

<link rel="shortcut icon" href="/favicon.ico">

<link rel="stylesheet" type="text/css" href="/static/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/static/css/bootstrap-responsive.min.css">
<link rel="stylesheet" type="text/css" href="/static/css/jquery.ui.all.css">
<link rel="stylesheet" type="text/css" href="/static/css/jquery.ui.theme.css">
<link rel="stylesheet" type="text/css" href="/static/css/selectmenu.css">
<link rel="stylesheet" type="text/css" href="/static/css/styles.css">
<link rel="stylesheet" type="text/css" href="/static/css/news/styles.css">

</head>
<body id="body1">

<div id="SiteWrapper">

<div id="Menu">
<a href="/"><svg width="300px" height="200px">
		<g transform="translate(47, 0) scale(2,2)">
		<path fill="#8F986A" fill-opacity="0.5" d="M24.621,38.702c3.335,5.891,8.069,7.551,14.781,11.458c3.43,1.993,6.886,4.428,10.162,7.054    c0.598-0.462,1.187-0.909,1.753-1.332c-4.367-3.254-7.719-5.319-8.744-5.935c0,0-2.921-2.701-9.554-14.481    c-6.632-11.769-7.32-15.31-7.32-15.31C21.762,24.769,20.587,31.546,24.621,38.702z" data-part-id="logo__item--logo_0__0"></path>
		<path fill="#8F986A" fill-opacity="0.5" d="M58.295,65.087c6.231,6.322,13.741,20.251,16.784,24.969c3.042,4.708,8.325,5.87,3.491-3.873    c-6.138-12.394-17.193-22.557-25.504-28.97c-0.592,0.474-1.175,0.957-1.753,1.442C53.806,60.747,56.171,62.928,58.295,65.087z" data-part-id="logo__item--logo_0__1"></path>
		<path fill="#8F986A" fill-opacity="0.5" d="M24.062,86.183c-4.832,9.743,0.45,8.581,3.493,3.873c3.042-4.718,10.551-18.646,16.776-24.969    c2.126-2.159,4.491-4.34,6.983-6.432c-0.577-0.485-1.16-0.968-1.75-1.441C41.258,63.629,30.208,73.791,24.062,86.183z" data-part-id="logo__item--logo_0__2"></path>
		<path fill="#8F986A" fill-opacity="0.5" d="M63.225,50.16c6.718-3.906,11.446-5.567,14.781-11.458c4.045-7.156,2.865-13.934-1.083-18.546    c0,0-0.677,3.541-7.31,15.31c-6.633,11.78-9.553,14.481-9.553,14.481c-1.027,0.616-4.378,2.681-8.744,5.935    c0.566,0.423,1.153,0.868,1.75,1.331C56.342,54.586,59.796,52.15,63.225,50.16z" data-part-id="logo__item--logo_0__3"></path>
		<path fill="#8F986A" fill-opacity="0.5" d="M51.313,58.655c0.578-0.485,1.161-0.969,1.753-1.442c-0.597-0.463-1.184-0.908-1.75-1.331    c-0.566,0.423-1.155,0.87-1.753,1.332C50.154,57.688,50.736,58.17,51.313,58.655z" data-part-id="logo__item--logo_0__4"></path>
		<path fill="#8F986A" fill-opacity="0.5" d="M51.313,58.655c0.578-0.485,1.161-0.969,1.753-1.442c-0.597-0.463-1.184-0.908-1.75-1.331    c-0.566,0.423-1.155,0.87-1.753,1.332C50.154,57.688,50.736,58.17,51.313,58.655z" data-part-id="logo__item--logo_0__5"></path>	
	<path fill="#335E87" fill-opacity="0.5" d="M66.634,30.27c0-7.57-12.791-23.677-14.489-25.771v21.882l2.709-1.607l6.262-3.725l-8.971,7.406v8.556   l3.84-2.288l7.289-4.338l-11.129,9.205v5.331l0.002,0.006C60.166,44.823,66.634,38.302,66.634,30.27z" data-part-id="logo__item--logo_0__6"></path>
	<path fill="#335E87" fill-opacity="0.5" d="M48.701,44.709l-0.36,5.265h2.603l0.002-0.006v-5.04V11.624V4.5v0C49.25,6.597,36.464,22.701,36.464,30.27   C36.464,37.534,41.758,43.547,48.701,44.709z" data-part-id="logo__item--logo_0__7"></path>
	</g>
<!-- <text data-part-id="logo__item--business" dy="0" dominant-baseline="auto" alignment-baseline="auto" font-size="32px" fill="#8F986A" letter-spacing="0" font-weight="normal" font-style="normal" data-font-family="Montserrat" data-font-weight="bold" data-font-style="normal">WK</text> -->
<g transform="translate(40, 0) scale(2,2)">
<text x="0" y="100" font-family="Times New Roman" fill="#8F986A" font-weight="normal" letter-spacing="0" font-size="64px">WK</text>
</g>
</svg></a>
<div id="menu"><ul class="level-1">
<li class=" first"><a href="/">About</a></li>
<li class=""><a href="/gallery">Gallery</a></li>
<li class=""><a href="/jobs">Previous Jobs</a></li>
<li class=""><a href="/pricing">Pricing</a></li>
<li class="listitem">Phone: +44-123-4567-890</li>
<li class=""><a href="mailto:{{.Email}}">Email: {{.Email}}</a></li>

</ul>

</div>

</div><!-- // Menu -->

<div id="Body">

<div id="Content" class="content">

<h1>{{.Title}}</h1>
<p style="text-align: center;">&nbsp;</p>
<p>{{.PrefixText}}</p>

{{range .Jobs}}
	<div class="job">
		<h2>{{.Title}}</h2>
		<p>{{.Description}}</p>
		<p>Please contact for references</p>
	</div>
{{end}}
<!-- <div class="job">
<h2>6 weeks in Ibiza cooking for a group of 8 people</h2>
<p>The job consisted of three meals a day, 2 courses for dinner and occasional packed lunches</p>
<p>Please contact for references</p>
</div>
<div class="job">
<h2>3 days in The Ritz</h2>
<p>Required to pluck 240 chickens over night for the morning breakfast</p>
<p>Please contact for references</p>
</div> -->

</div><!-- // Content -->

<div id="Social">

<p>
{{.Phone}}<span class="separator">-</span>
<a href="mailto:{{.Email}}" class="email">{{.Email}}</a>
<!-- <span class="separator">-</span>
<a href="https://www.facebook.com/willkeith" target="_blank" class="social facebook"><img src="static/img/icon-facebook.png" alt="f" width="30" height="30"></a> -->
</div>

</div>

</div>

</body><div></div></html>